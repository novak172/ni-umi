(define (domain shakey)

        (:types
        room robot box switch
    )

  

    (:predicates
        (can_move ?r1 - room ?r2 - room)
        (in_room ?room ?obj ) ; bez typu - pouzivam pro robota, switche i boxy
        (is_carrying ?box - box ?shakey - robot)
        (on_box ?shakey - robot)
        (is_on ?switch - switch)
    )

  
    (:action move
        :parameters (?start - room ?dest - room ?shakey - robot)
        :precondition (and (can_move ?start ?dest) (in_room ?start ?shakey) (not (on_box ?shakey)))
        :effect (and 
        (not (in_room ?start ?shakey))
        (in_room ?dest ?shakey) 
        )
    )
    
    (:action pick_up
        :parameters (?box - box ?room - room ?shakey - robot)
        :precondition (and (in_room ?room ?shakey) (in_room ?room ?box))
        :effect (and (is_carrying ?box ?shakey) (not (in_room ?box ?room)))
    )
    
    (:action put_down
        :parameters (?box - box ?room - room ?shakey - robot)
        :precondition (and (in_room ?room ?shakey) (is_carrying ?box ?shakey))
        :effect (and (not (is_carrying ?box ?shakey)) (in_room ?room ?box))
    )
    
    (:action climb_up
        :parameters (?box - box ?room - room ?shakey - robot)
        :precondition (and (in_room ?room ?box) (in_room ?room ?shakey))
        :effect (on_box ?shakey)
    )
    
    (:action climb_down
        :parameters (?shakey - robot)
        :precondition (on_box ?shakey)
        :effect (not (on_box ?shakey))
        )
        
    (:action turn_on
        :parameters (?switch - switch ?shakey - robot ?box - box ?room - room)
        :precondition (and (in_room ?room ?switch) (in_room ?room ?box) (in_room ?room ?shakey) (on_box ?shakey) (not (is_on ?switch)))
        :effect (is_on ?switch)
    
    )
    
    (:action turn_off
        :parameters (?switch - switch ?shakey - robot ?box - box ?room - room)
        :precondition (and (in_room ?room ?switch) (in_room ?room ?box) (in_room ?room ?shakey) (on_box ?shakey) (is_on ?switch))
        :effect (not (is_on ?switch))
    
    )
        
        
    
    
)