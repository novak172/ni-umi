## Shakey

### zadani


Robot Shakey stál u zrodu klasického plánovaní (STRIPS). Navrhněte doménu pro Shakeyho,
který působí v následujícím prostředí:

![shakey start](shakey-start.png)


Shakey se může pohybovat mezi místnostmi a chodbou. Může rozsvítit nebo zhasnout
v místnosti pomocí vypínače. Na vypínač ale ze země nedosáhne, musí se vyšplhat na krabici,
aby dosáhnul (skutečný Shakey toto neuměl). Krabice může Shakey převážet, uveze jednu.
V navržené plánovací doméně nalezněte plán, jehož vykonáním Shakey rozsvítí v místností 4
(Room 4). Opět můžete plán hledat pomocí existující plánovače.