(define (problem shakey)

    (:domain shakey)
    
    
    (:objects 
    room1 - room
    room2 - room 
    room3 - room
    room4 - room
    box1 - box
    box2 - box
    box3 - box
    box4 - box
    switch1  - switch
    switch2  - switch
    switch3  - switch
    switch4  - switch
    corridor - room
    shakey - robot
    )
    
    
    
    (:init
    
       
    (is_on switch2)
    (is_on switch3)
    (can_move room1 corridor) 
    (can_move corridor room1)
    
    (can_move room2 corridor)
    (can_move corridor room2)
    
    (can_move room3 corridor) 
    (can_move corridor room3)
    
    (can_move room4 corridor) 
    (can_move corridor room4)
           
    (in_room room1 box1)
    (in_room room1 box2)
    (in_room room1 box3)
    (in_room room1 box4)
    (in_room room1 switch1)
    (in_room room2 switch2)
    (in_room room3 switch3)
    (in_room room4 switch4)
    (in_room room3 shakey)
    )
    
    (:goal (is_on switch4))
)