
Vstupní mapu jsem vytvořil na [sokoban builder](https://www.sokobanonline.com/)

Pro vygenerování mapy ve formátu pro PDDL jsem použil script v sokoban-map-gen.ipynb (není nijak zajímavý, přikládam jen pro úplonost).

Sokoban vyžaduje, aby byla celá mapa obehnána zdmi - v PDDL jsem hraniční zdi vynechal - nejsou potřeba (mapa má o políčko menší rozměry)

![sokoban mapa](sokoban-map.png)
