(define (domain sokoban)
  (:predicates (has_player ?x)
               (has_box ?x)
               (adjacent_horiz ?x ?y)
               (adjacent_vert ?x ?y)
               (has_wall ?x)
               )
  (:action move
           :parameters (?x ?y) ; pohyb z mista ?x do mista ?y
           :precondition (and 
           
                              (or (adjacent_horiz ?x ?y) (adjacent_vert ?x ?y))
                              (not (has_box ?y))
                              (not (has_wall ?y))
                              (not (has_wall ?x))
                              (has_player ?x))
          
           :effect		   (and (has_player ?y)
                              (not (has_player ?x))))
  (:action push-box
		   :parameters (?x ?y ?z) ;?x - pozice hrace , ?y pozice krabice , ?z - cilova pozice
		   :precondition (and 
		                  (has_player ?x)
                          (has_box ?y)
                          (not (has_box ?z))
                          (not (has_wall ?z))
                          (not (has_wall ?x))
                          (not (has_wall ?y))
                          (not (has_player ?z))
                          
                          (or 
                              (and (adjacent_horiz ?x ?y) (adjacent_horiz ?y ?z) )
                              (and (adjacent_vert ?x ?y)  (adjacent_vert ?y ?z)  )
                          )
                          
                          
                          
                          
                          
    )
	       :effect (and (has_box ?z)
						(not(has_box ?y))
						(has_player ?y)
						(not(has_player ?x))))
  )