(define (domain hm-monkey)

    (:requirements :typing)

    (:types
        location box monkey bananas
    )

    (:predicates
     (is_at ?place -location ?obj)
     (is_on_box ?monkey - monkey)
     (has_banana ?monkey - monkey)
     (movable ?box - box)
     
     
    
    )

    (:action Go
        :parameters (?source - location ?dest - location ?monkey - monkey)
        :precondition (and (is_at ?source ?monkey) (not (is_on_box ?monkey)))
        :effect (and (is_at ?dest ?monkey)  (not (is_at ?source ?monkey)))
    )
    
    
    
    (:action Push
        :parameters (?monkey - monkey ?box - box ?source - location ?dest - location)
        :precondition ( and (movable ?box) (is_at ?source ?monkey) (is_at ?source ?box) (not (is_on_box ?monkey)))
        :effect (and (not (is_at ?source ?box)) (not (is_at ?source ?monkey)) (is_at ?dest ?box) (is_at ?dest ?monkey))
    )
    
    
    (:action ClimbUp
        :parameters (?monkey - monkey ?location - location ?box - box)
        :precondition (and (is_at ?location ?monkey) (is_at ?location ?box) (not (is_on_box ?monkey)))
        :effect (is_on_box ?monkey)
    )
    
    (:action climbDown
        :parameters (?monkey - monkey)
        :precondition (is_on_box ?monkey)
        :effect (not (is_on_box ?monkey))
    )
    
    (:action Grasp
        :parameters (?bananas - bananas ?monkey - monkey ?location - location)
        :precondition (and (is_on_box ?monkey) (is_at ?location ?bananas) (is_at ?location ?monkey))
        :effect (has_banana ?monkey)
    )
    
    (:action UnGrasp
        :parameters (?monkey - monkey ?bananas - bananas)
        :precondition (has_banana ?monkey)
        :effect (not (has_banana ?monkey))
    )

)