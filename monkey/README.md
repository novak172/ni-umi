## monkey

### zadání
Navrhněte plánovací doménu pro úlohu s opicí a vysoko visícími banány. Opice se může
pohybovat mezi místy A, B, a C, které jsou rozmístěny v lince. Banány jsou na místě B. Dále
v úloze vystupuje krabice, která je umístěná na místě A. Opice ze země na banány nedosáhne,
ale může vylézt na krabici, ze které už na banány dosáhne. Krabicí opice může posouvat, 
přitom ale musí být na stejném místě jako krabice. Následující ilustrace ukazuje v jaké situaci
se opice na začátku nachází.


a) V navržené doméně sestavte plán, kterým se opice zmocní banánů.
b) Upravte cíl tak, aby opice zmátla vědce, kteří ji pozorují, tj. zmocnila se banánů, ale
zároveň uvedla prostředí do původního stavu (krabice zpět na své místo).
c) Úlohu řešte automaticky pomocí nějakého plánovače účastnícího se soutěže IPC.
d) V prostředí se bude vyskytovat více krabic dvou typů, a sice lehké a těžké. S těžkými
opice nedokáže pohnout s lehkými ano. Modifikujte doménu, aby tyto vlastnosti
operátory zohledňovaly.

---

dodatek k řešení: 
Doména je společná pro všechny 3. verze problémů. (základní i "confusion" varianta proto obsahují krabici splňující predikát "movable")