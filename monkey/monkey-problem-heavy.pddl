(define (problem monkey-heavy)

    (:domain hm-monkey)
    
    
    (:objects 
    a - location
    b - location
    c - location
    d - location
    monkey - monkey 
    bananas - bananas
    light_box - box
    heavy_box - box
    )
    
    
    
    (:init
    
    (is_at a monkey)
    (is_at b bananas)
    (is_at d light_box)
    (is_at c heavy_box)
    (movable light_box)
    )
    
    (:goal (has_banana monkey))
)