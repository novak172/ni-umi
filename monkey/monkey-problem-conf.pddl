(define (problem confusion-of-da-highest-orda)

    (:domain hm-monkey)
    
    
    (:objects 
    a - location
    b - location
    c - location
    monkey - monkey 
    bananas - bananas
    box - box
    )
    
    
    
    (:init
    
    (is_at a monkey)
    (is_at b bananas)
    (is_at c box)
    (movable box)
    )
    
    (:goal (and(has_banana monkey) (is_at c box) (is_at a monkey)))
)